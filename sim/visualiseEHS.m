function visualiseEHS

	mi = 1.00000037 * 4e-7*pi;
	f = 100e6 % Mhz
	w = 2*pi*f;
	c = 3e8;
	k = w / c
	I = 1; % A
	h = 0.01 * pi;
	r = 1
	% Looking down from above
	[x, y] = meshgrid (1:2:20);

	figure(1);clf;

	positions = [];
	vectors = [];

	theta = pi/2;
	for phi = [0:7] * 2*pi/8
%		[H_ded_polar, _] = ded_polar (h, k, I, position)
%		ded_p_cart = polar_to_cartesian(position)
%		ded_H_cart = polar_to_cartesian(H_ded_polar)
%		quiver(ded_p_cart(1), ded_p_cart(2), ded_p_cart(3), ...
%		       ded_H_cart(1), ded_H_cart(2), ded_H_cart(3));
%		hold on;
		positions(end+1,:) = [r, theta, phi];
		positions(end+1,:) = [2, theta, phi];
%		positions(end+1,:) = polar_to_cartesian(position)
%		vectors(end+1,:)   = ded_H_cart
	end

	p_cart = polar_to_cartesian(positions)
	[ded_H_cart, ded_E_cart] = ded_cart(h, k, I, positions)

%	range=-5:5; [x, y, z] = meshgrid(range, range, range)
%	ded_H_cart = abs(ded_H_cart)
	ded_H_cart = imag(ded_H_cart)
%	quiver(positions(:,1), positions(:,2), vectors(:,1), vectors(:,2))
%	hq = quiver(p_cart(:,1), p_cart(:,2), ...
%	            ded_H_cart(:,1), ded_H_cart(:,2));

	plot3([0, 0], [0, 0], h*[-1/2, 1/2], 'r')
	hold on;
	hq = quiver3(p_cart(:,1), p_cart(:,2), p_cart(:,3),...
	             ded_H_cart(:,1), ded_H_cart(:,2), ded_H_cart(:,3));
	set (hq, "maxheadsize", 0.33);

%	ppolar_to_cartesian([5, pi/2, asin(3/5)], [1, pi/2, pi/2])
end

% Dinamicni elektricni dipol
function [H, E] = ded_cart (h, k, I, positions)
	r     = positions(:,1);
	theta = positions(:,2);
	phi   = positions(:,3);
	H = [-sin(phi), cos(phi), zeros(length(r), 1)];
	H = H .* (I.*h ./ (4.*pi) .* (j*k./r + 1./r./r) .* sin(theta));
%	H = H .* (I.*h ./ (4.*pi) .* exp(-j*k.*r) .* (j*k./r + 1./r./r) .* sin(theta));
	E = 0;
end

% Dinamicni elektricni dipol
function [H, E] = ded_polar (h, k, I, position)
	r     = position(1);
	theta = position(2);
	phi   = position(3);
	H = [0, 0, I*h / (4*pi) * exp(-j*k*r) * (j*k/r + 1/r/r) * sin(theta)];
	E = 0;
end

function cart = polar_to_cartesian (vector)
	phi   = vector(:,3);
	theta = vector(:,2);
	r     = vector(:,1);

	cart = [r.*sin(theta).*cos(phi), ...
	        r.*sin(theta).*sin(phi), ...
	        r.*cos(theta)];
end

function cart = ppolar_to_cartesian (position, vector)
	pphi   = position(3);
	ptheta = position(2);
	pr     = position(1);

	x = pr*sin(ptheta)*cos(pphi)
	y = pr*sin(ptheta)*sin(pphi)
	z = pr*cos(ptheta)

	ur     = [x, y, z] / sqrt(x^2 + y^2 + z^2)
	utheta = [x*z, y*z, -(x^2+y^2)]/ sqrt(x^2 + y^2 + z^2)/sqrt(x^2 + y^2)
	uphi   = [-y, x, 0] / sqrt(x^2 + y^2)

	phi   = vector(3);
	theta = vector(2);
	r     = vector(1);

	cart = r*ur + theta*utheta + phi*uphi;
end
